// problem1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <limits>

using namespace std;

string cmd;
double number;

void computeSum(vector<double> &A)
{
	double sum = 0;
	for (int i = 0; i < A.size(); i++)
	{
		sum += A[i];
	}
	cout << sum << endl;
}
void computeAvg(vector<double> &A)
{
	double sum = 0;
	double avg = 0;
	for (int i = 0; i < A.size(); i++)
	{
		sum += A[i];
	}
	avg = sum / A.size();
	cout << avg << endl;
}
void computeMax(vector<double> &A)
{
	int max = numeric_limits<int>::min();
	for (int i = 0; i < A.size(); i++)
	{
		if (A[i] > max)
		{
			max = A[i];
		}
	}
	cout << max << endl;
}
void computeMin(vector<double> &A)
{
	int min = numeric_limits<int>::max();
	for (int i = 0; i < A.size(); i++)
	{
		if (A[i] < min)
		{
			min = A[i];
		}
	}
	cout << min << endl;
}
void computeIns(vector<double> &A)
{
	cin >> number;
	A.push_back(number);
}
void computeDel(vector<double> &A)
{
	cin >> number;
	A.erase(A.begin() + (number - 1));
}


void execute_cmd(vector<double> &A)
{
	if (cmd == "sum")
	{
		computeSum(A);
	}
	else if (cmd == "avg")
	{
		computeAvg(A);
	}
	else if (cmd == "min")
	{
		computeMin(A);
	}
	else if (cmd == "max")
	{
		computeMax(A);
	}
	else if (cmd == "ins")
	{
		computeIns(A);
	}
	else if (cmd == "del")
	{
		computeDel(A);
	}
	else
	{
		cout << "Unrecognizable command." << endl;
	}

}

int main()
{
	int n;
	cout << "Enter how many elements will the vector have:" << endl;
	cin >> n;
	
	vector<double> v(n);
	for (int  i = 0; i < v.size(); i++)
	{
		cin >> v[i];
	}
	cout << "Enter command:" << endl;
	
	while (cin >> cmd)
	{
		execute_cmd(v);
	}

    return 0;
}

